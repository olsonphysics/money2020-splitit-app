MID = "8NWJWG54FFY5J"
API_TOKEN = "5de57bb4-6569-ecb9-8f07-ba16ae754a35"
NUM_ORDERS = 50
ENVIRONMENT = "https://sandbox.dev.clover.com/" 

cardNumber = '6011361000006668'
expMonth = 12
expYear = 2020
CVV = 123
orderId = "TR631REMBYH7P"

from flask import Flask, request, Response
import requests
import json
from random import randint
import sys
from time import sleep
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from base64 import b64encode

def payment(price, orderId):
	########## BEGIN PAYMENT ##########
    # create a cipher from the RSA key and use it to encrypt the card number, prepended with the prefix from GET /v2/merchant/{mId}/pay/key
    cipher = PKCS1_OAEP.new(RSAkey)
    encrypted = cipher.encrypt(prefix + cardNumber)

    # Base64 encode the resulting encrypted data into a string to use as the cardEncrypted' property.
    cardEncrypted = b64encode(encrypted)

    post_data = {
        "orderId": orderId,
        "currency": "usd",
        "amount": long(price),
        "expMonth": expMonth,
        "cvv": CVV,
        "expYear": expYear,
        "cardEncrypted": cardEncrypted,
        "last4": cardNumber[-4:],
        "first6": cardNumber[0:6]
    }

    posturl = ENVIRONMENT + "v2/merchant/" + MID + '/pay'
    sleep(0.1)
    response = requests.post(
        posturl,
        headers = headers,
        data= post_data
        )
    if response.status_code != 200:
        print "Something went wrong during developer pay"
        sys.exit()
		
		
def checktotals(orderId):
	otherurl = ENVIRONMENT + "v3/merchants/" + MID + "/orders/" + orderId
	oourl= otherurl + "/payments/"
	response = requests.get(otherurl, headers=headers)
	tbill=response.json()[u'total']
	response = requests.get(oourl, headers=headers)
	dummy= response.json()[u'elements']
	holder=[]
	holder.append(int(tbill))
	for i in range(len(dummy)):
		holder.append(int(dummy[i][u'amount']))
	return(holder)

#def checkarray(arr):
#	return (arr[0]-sum(arr[1:]))
#	tbill=response.json()[u'total']

#	totalbill=response[u'total']
#	payinfo=response[u'payments'][u'elements']
#	paysum=0
#	for i in payinfo:
#		paysum+=int(payinfo[u'amount'])
	
#	return( "$" + string(float(paysum)/100) + "of $" + string(float(totalbill)/100) + "has been payed.")
		

	
# Fetch developer pay secrets from GET /v2/merchant/{mId}/pay/key
url = ENVIRONMENT + "v2/merchant/" + MID + '/pay/key'
headers = {"Authorization": "Bearer " + API_TOKEN}
response = requests.get(url, headers = headers)
if response.status_code != 200:
    print "Something went wrong fetching Developer Pay API secrets"
    sys.exit()
response = response.json()
	
modulus = long(response['modulus'])
exponent = long(response['exponent'])
prefix = str(response['prefix'])

RSAkey = RSA.construct((modulus, exponent))

app = Flask(__name__)
@app.route('/')
def index():
	return 'Hello world'

@app.route('/pay', methods=['GET'])
def show_post():
	price = request.args.get('price')
	orderId = request.args.get('id')
	arr2= checktotals(orderId)
	remaining = (arr2[0]-sum(arr2[1:]))
	if remaining>int(price):
		requests.get("http://192.168.43.29:8080/off")
	if remaining>int(price):
		payment(int(price),orderId)
		return str(checktotals(orderId))
	elif remaining == int(price):
		payment(int(price),orderId)
		requests.get("http://192.168.43.29:8080/on")
		return Response("{'a':'b'}", status=201, mimetype='application/json')
	return str(arr2)
@app.route('/light', methods=['GET'])
def turn_on():
	requests.get("http://192.168.43.29:8080/on")
	
if __name__ == '__main__':
	app.run(threaded=True, debug=True, host='0.0.0.0')
	

