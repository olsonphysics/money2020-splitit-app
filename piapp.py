from flask import Flask
import RPi.GPIO as GPIO


app=Flask(__name__)

led_pin=18

GPIO.setmode(GPIO.BOARD)
GPIO.setup(led_pin, GPIO.OUT)
GPIO.output(led_pin, 0)

@app.route('/')
#def index():
#	return "Hello World"

@app.route('/<state>')
def set_pin(state):
	if(state=='on'):
		GPIO.output(led_pin,1)
	if(state=='off'):
		GPIO.output(led_pin,0)

	

if __name__=='__main__':
	app.run(debug=False, host='0.0.0.0', port=8080)