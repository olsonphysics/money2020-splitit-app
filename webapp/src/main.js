require("./styles/main.less");

var a = 12;
var square = (x) => x*x; 

document.addEventListener("DOMContentLoaded", () => {
	// document.querySelector("#var").innerHTML = a;
	// document.querySelector("#result").innerHTML = square(a);
	var ORDER_ID;
    var URL_ORDER = 'https://divee.herokuapp.com/order?id=' + ORDER_ID;

    // $.get( URL_ORDER, function( data ) {
     
    //     $("h2").text("Bill: " + data.id);
    //     $.each(data.lineItems.elements, function(k,v) {
    //       $("ul").append('<li id="'+v.id+'">'+v.name + " - <span class=\"price\">" + v.price+'</span></li>');
    //     })
    // });

    $("form[name=order]").submit(function(event) {
      var orderId = $("input[name=orderId]").val();
      ORDER_ID = orderId;
      console.log(orderId);
      $.get( 'https://divee.herokuapp.com/order?id=' + orderId, function( data ) {
      	$("h2").text("Bill: " + data.id);
        $.each(data.lineItems.elements, function(k,v) {
          $("ul").append('<li id="'+v.id+'">'+v.name + " - <span class=\"price\">" + v.price+'</span></li>');
        })
      });
      // sendPayment(ORDER_ID, pay);
      event.preventDefault();
    });

    $("form[name=payment]").submit(function(event) {
      var pay = $("input[name=payment]").val();
      console.log(pay);
      console.log(ORDER_ID);
      sendPayment(ORDER_ID, pay);
      event.preventDefault();
    });

    function getOrder(orderId) {

    }

    function sendPayment(orderId, amount) {
      console.log(orderId + amount)
      var URL_PAY = 'http://192.168.43.14:5000/pay?'
      var PRICE = 'price=' + amount +'&';
      var ORDER = 'id=' + ORDER_ID;
      var URL = URL_PAY + PRICE + ORDER;
      console.log(URL);
      $.get(URL, function(data) {
        console.log(data);
        $('#paid').text('PAID');
      })
    }
});
